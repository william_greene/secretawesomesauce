<?php
require('./includes/charities.php');

$title = 'Create A Campaign';
$bodyId = 'create';

include('./includes/header.php');
?>

<div class="span-24">
	<h1 class="center">Create A Pledge Campaign</h1>
	<h2 class="center">Cras justo odio, dapibus ac facilisis in, egestas eget quam.</h2>
</div>

<form method="post" action="create-share.php">
<div class="span-24">

<?php
	// Loop through each of the charity IDs in $charities
	for ($i = 0, $iMax = sizeof($charities); $i < $iMax; $i++) {
		$charity = $charities[$i];
		if (!empty($charity)) {
?>
	<div class="span-7">
		<h2><?= $charity['name'] ?></h2>
		<img src="<?= $charity['imgBig'] ?>" alt="<?= htmlspecialchars($charity['name']) ?>" width="270" height="151">
		<p><?= $charity['desc'] ?></p>
		<input type="checkbox" id="" name="" value="" />
	</div>
<?php
		}
	}
?>
</div>
<div class="span-24">
	<input type="submit" value="Create">
</div>
</form>

<?php include('./includes/footer.php'); ?>