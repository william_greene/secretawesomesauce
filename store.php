<?php
require('./includes/charities.php');
require('./includes/common.php');

$campaign = $_REQUEST['campaign'];
$c = $_REQUEST['c'];	// A campaign's chosen charities

// If no store name (campaign) set or if charities not specified, redirect to index
if (empty($campaign) || empty($c) || (is_array($c) == false)) {
	header('Location: /');
}

// If a charity is specified, set the Facebook preview image
if (!empty($_REQUEST['s'])) {
	$previewTitle = $charities[(int) $_REQUEST['s']]['name'];
	$description = $charities[(int) $_REQUEST['s']]['desc'];
	$previewImage = $charities[(int) $_REQUEST['s']]['imgSmall'];
}

$title = $campaign.'’s Favorite Charities';
$bodyId = 'home';

include('./includes/header.php');
?>

<div class="span-24">
	<h1 class="center"><img src="<?= getTwitterAvatar($campaign) ?>" alt=""> <?= $campaign ?>’s Favorite Charities</h1>
	<h2 class="center">Cras justo odio, dapibus ac facilisis in, egestas eget quam.</h2>
</div>

<div class="span-24">
<?php
	// Loop through each of the charity IDs in $charities
	for ($i = 0, $iMax = sizeof($c); $i < $iMax; $i++) {
		$charity = $charities[$c[$i]];
		if (!empty($charity)) {
?>
	<div class="span-7">
		<h2><?= $charity['name'] ?></h2>
		<img src="<?= $charity['imgBig'] ?>" alt="<?= htmlspecialchars($charity['name']) ?>" width="270" height="151">
		<p><?= $charity['desc'] ?></p>
		<form method="post" action="/thanks.php">
			<label for="amt<?= $c[$i] ?>">Pledge Amount</label><br />
			<select name="amt" id="amt<?= $c[$i] ?>">
				<option value="0.02">$0.02</option>
				<option value="1.12">$1.12</option>
				<option value="3.57">$3.57</option>
				<option value="12.19">$12.19</option>
				<option value="1000000.02">$1,000,000.02</option>
			</select>
			<input name="c" type="hidden" value="<?= $c[$i] ?>">
			<input type="submit" value="Pledge">
			<p class="small">disclosure of some sort that IRS and blah</p>
		</form>
		<div class="center">
			twitter • facebook • email
		</div>
	</div>
<?php
		}
	}
?>	
</div>

<?php include('./includes/footer.php'); ?>