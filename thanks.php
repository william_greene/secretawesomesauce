<?php
require('./includes/charities.php');

if (empty($_REQUEST['c']) && ($_REQUEST['c'] != 0)) {
	header('Location: /');
} else {
	$charity = $charities[(int) $_REQUEST['c']];
}

$title = 'Thanks for supporting TODO';
$bodyId = 'thanks';
include('./includes/header.php');

?>

<div class="span-24">
	<h1>Thanks for your support!</h1>
	<pre><?php echo print_r($_REQUEST, true); ?></pre>
	
	<h2><?= $charity['name'] ?></h2>
	
	<img src="<?= $charity['imgEnd'] ?>" alt="<?= htmlspecialchars($charity['name']) ?>" width="270" height="151">
</div>

<?php include('./includes/footer.php'); ?>