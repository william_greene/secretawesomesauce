<?php

function getTwitterAvatar($twitterUserName) { 
	$options = array(
		CURLOPT_HEADER			=> TRUE,	// Final URL returned in the header 
		CURLOPT_FOLLOWLOCATION	=> TRUE, 	// We need the ultimate destination 
		CURLOPT_NOBODY			=> TRUE,
		CURLOPT_RETURNTRANSFER	=> TRUE,
		CURLOPT_CONNECTTIMEOUT	=> 3, 	 	// Keep small to not delay page rendering 
		CURLOPT_TIMEOUT			=> 3,  		// Keep small to not delay page rendering
		CURLOPT_MAXREDIRS		=> 5		// Shouldn't be more than 1 redirect, but just in case
	); 
	
	$ch = curl_init('http://api.twitter.com/1/users/profile_image/'.$twitterUserName.'.json');
	curl_setopt_array($ch, $options);
	$response = curl_exec($ch);
	$header = curl_getinfo($ch);
	curl_close($ch);
	
	$imgSrc = $header['url'];

	if (strrpos($imgSrc, 'http') !== FALSE) {
		// Return Twitter avatar
		return $imgSrc;
	} else {
		// Return generic image
		return 'https://si3.twimg.com/sticky/default_profile_images/default_profile_4_reasonably_small.png';
	}
}