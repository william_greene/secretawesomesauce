<?php

$charities = array();
$charities[] = array(
	'name'		=> 'Charity 0',
	'desc'		=> 'Maecenas faucibus mollis interdum.',
	'imgBig'	=> 'static/img/charity0b.png',
	'imgSmall'	=> 'static/img/charity0s.png',
	'imgEnd'	=> 'static/img/charity0f.png'
);

$charities[] = array(
	'name'		=> 'Charity 1',
	'desc'		=> 'Etiam porta sem malesuada magna mollis euismod.',
	'imgBig'	=> 'static/img/charity1b.png',
	'imgSmall'	=> 'static/img/charity1s.png',
	'imgEnd'	=> 'static/img/charity1f.png'
);

$charities[] = array(
	'name'		=> 'Charity 2',
	'desc'		=> 'Duis mollis, est non commodo luctus, nisi erat porttitor ligula.',
	'imgBig'	=> 'static/img/charity2b.png',
	'imgSmall'	=> 'static/img/charity2s.png',
	'imgEnd'	=> 'static/img/charity2f.png'
);

$charities[] = array(
	'name'		=> 'Charity 3',
	'desc'		=> 'Donec id elit non mi porta gravida at eget metus.',
	'imgBig'	=> 'static/img/charity3b.png',
	'imgSmall'	=> 'static/img/charity3s.png',
	'imgEnd'	=> 'static/img/charity3f.png'
);

$charities[] = array(
	'name'		=> 'Charity 4',
	'desc'		=> 'Aenean lacinia bibendum nulla sed consectetur.',
	'imgBig'	=> 'static/img/charity4b.png',
	'imgSmall'	=> 'static/img/charity4s.png',
	'imgEnd'	=> 'static/img/charity4f.png'
);