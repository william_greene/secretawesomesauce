<?php
	session_start(); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title><?= $title ?></title>
<?php if (!empty($previewTitle)) { ?>	<meta property="og:title" content="<?= $previewTitle ?>"/>
<?php } ?>
<?php if (!empty($description)) { ?>	<meta property="og:description" content="<?= $description ?>"/>
	<meta name="description" content="<?= $description ?>" />
<?php } ?>
<?php if (!empty($previewImage)) { ?>	<meta property="og:image" content="<?= $previewImage ?>" />
<?php } ?>
	<link rel="stylesheet" media="screen" href="static/css/screen.css" />
	<link rel="stylesheet" media="print" href="static/css/print.css" />
	<link rel="shortcut icon" href="/favicon.ico" />
	<link rel="apple-touch-icon" href="/apple-touch-icon.png" />
	<!--[if lt IE9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body id="<?= $bodyId ?>" class="container">