<?php
/*
 *	XHR endpoint for querying a Twitter user photo
 */

require('./includes/common.php');

if (empty($_REQUEST['user'])) {
	echo 'empty';
} else {
	echo getTwitterAvatar($_REQUEST['user']);
}